---
version: 2
type de projet: Projet de bachelor
année scolaire: 2020/2021
titre: Plateforme d’analyse d’image pour détection des tissus sains et non sains
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 1
mandants: 
  - Département de médecine/pathologie UNIFR - Dr. Jimmy Stalin et Prof. Curzio Ruegg
professeurs co-superviseurs:
  - Elena Mugellini
  - Cyril Berton
  - Karl Daher
proposé par étudiant: Mathieu Lamon
mots-clés: [Image Anylsis, Color Analysis, Anomaly Detection]
langue: [F,E]
confidentialité: non
suite: oui
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/cell.png}
\end{center}
```

## Description/Contexte

Dans le cadre d’une étude en pathologie médicale, nous souhaitons développer une plateforme sur laquelle nous pourrions traiter et analyser des images des tissus pulmoniares. 
Le projet se concentre sur la détection des cellules ou tissus sain et pas-sains. Nous nous concentrons sur la détection de l’altération pulmonaire dans la fibrose et la détection de différents types de cellules dans les poumons. 
Le tissue est normalement coloré et scanné par un microscope. 
Type de coloration : 
-	Coloration rouge pour les tissus pas sains 
-	Coloration bleu violet pour les tissus pas sains
-	Coloration brune est utilisée pour la détection de cellules spécifiques  

Nous avons défini en premier lieu 1/ les sections d’intérêt (alvéole pulmonaire) puis de définir 2/ s'il s’agit de tissus sains ou pas-sains et 3/ la présence et la quantification de cellules d’intérêt pour le suivi de l’évolution de la maladie. 
Dans ce projet nous allons implémenter une platforme qui pourra nous aider à valider les analyses d'une manière rapide par un expert de recherche médical.

## Objectifs/Tâches
 
- Dévelopement de la platforme de visualisation 
- Amélioration de l'algorithme de détection
- Analyse des résultats de l'algorithme
