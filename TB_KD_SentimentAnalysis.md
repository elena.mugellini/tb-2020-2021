---
version: 2
type de projet: Projet de bachelor
année scolaire: 2020/2021
titre: Text Sentiment Analysis 
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 1

professeurs co-superviseurs:
  - Omar Abou Khaled
  - Elena Mugellini
  - Karl Daher
  - Didier Crausaz
proposé par étudiant: Jerome Vial
mots-clés: [text analysis, sentiment analysis, text processing]
langue: [F,E]
confidentialité: non
suite: oui
---

## Description/Contexte

Facebook, Twitter, même Gmail maintenant utilise l'analyse des sentiments das le texte (Sentiment analysis). Cette technologie est devenu utilisé dans plusieurs algorithmes et modèles en ligne afin de définir l'état des utilisateurs. Dans ce cadre nous cherchons à implémenter un pipeline de sentiment analysis dans laquelle nous pourrions détécter les sentiments dans un flux de texte. 
Une recherche doit être effectuer sur les différentes APIs qui existent. Une implémentation de ce pipeline doit être effectuer avec l'intégration d'un modèle de sentiment analysis.
 
Le texte à utiliser pourra être des commentaires de Facebook sur un post ou image, des conversations Twitter ou autre genre de texte. 
La visualitsation des tweets et des sentiments doit être implémenter.

## Objectifs/Tâches
 
- Implémentation de l'algorithme 
- Implémentation de la platforme de visualitsation
- Visualisation des sentiments 
- Visualitsation des phrases et les sentiments
