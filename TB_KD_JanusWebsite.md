---
version: 2
type de projet: Projet de bachelor
année scolaire: 2020/2021
titre: Website visit and visualitsation in JanusXR 
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 1

professeurs co-superviseurs:
  - Elena Mugellini
  - Karl Daher
mots-clés: [virtual reality, data transfer, development]
langue: [F,E]
confidentialité: non
suite: non
---

```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/janus.png}
\end{center}
```

## Description/Contexte

Le dévelopement des sites interactives est considéré une base importante pour attirer les utilisateurs. Avec les nouveautés technologiques comme la réalité augmentée et la réalité virtuelle, des nouveaux types d'interactions peuvent ajouter une valeur importante. Même la manière de présenter les données à evoluer. Dans ce projet nous déveloperons un site accessible par des casques VR ou par le browser. Le but est de tranférer un site existant sous une nouvelle forme, et une nouvelle représentation des données en utilisant JanuxXR ( https://www.janusvr.com/index.html ).   

JanusXR se présente comme un navigateur internet XR, pour ré-imaginer les pages webs.

Check it out: https://www.youtube.com/watch?v=8FCaFQkMsuc&ab_channel=Matteo311

## Objectifs/Tâches
 
- Apprendre JanusXR
- Creation des maquettes du site et la base de données
- Implémentation du site en JanusXR 
- Test et validation
