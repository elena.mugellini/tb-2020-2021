---
version: 2
type de projet: Travail de bachelor
année scolaire: 2020/2021
titre: Jeux artistiques en Réalité Virtuelle
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Elena Mugellini
  - Omar Abou Khaled
  - Marine Capallera
  - Karl Daher
mots-clés: [Gamification, games, Modelling, Réalité Virtuelle, Unity]
langue: [F,E]
confidentialité: non
suite: non
---
 
 
## Description/Contexte
De nos jours, les artistes ont du mal à se faire connaitre, même au sein de leur communauté. En parallèle, les jeux prennent de plus en plus de place dans la vie de tous. Enfants et adultes utilisent de plus en plus d’applications ludiques. Dans le but d’associer l’art et la technologie, ce projet va permettre de concevoir des jeux afin de mettre en avant les travaux d’un artiste local.
Un jeu de puzzle en 3D a déjà été développé au cours du précédent semestre. Le but de ce projet sera de créer un monde virtuel dans lequel il sera possible de découvrir des oeuvres d'artistes sous formes de mini-jeux et énigmes à résoudre.
 
 
## Objectifs
 
Durant ce projet de semestre, l’étudiant devra accomplir les tâches suivantes :
 
1. Prendre connaissance du projet existant
2. Apporter des améliorations à la solution existante
3. Implémenter la Réalité Virtuelle
4. Réaliser des tests fonctionnels et utilisateurs
5. Concevoir et implémenter d'autres activités 
 
## Contraintes
 
1. Utilisation de logiciel Unity
