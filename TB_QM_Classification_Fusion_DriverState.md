---
version: 2
type de projet: Travail de bachelor
année scolaire: 2020/2021
titre: Fusion de classificateurs pour la prédiction de l’état physiologique d’un conducteur
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Elena Mugellini
  - Omar Abou Khaled
  - Quentin Meteier
mots-clés: [Python, Machine Learning, classification, fusion, conduite autonome, physiologie]
langue: [F,E]
confidentialité: non
suite: non
---


## Description/Contexte

Le projet AdVitam, mené par l’Institut HumanTech en collaboration avec l’Université de Fribourg et l’He-Arc, vise à comprendre quelles sont les situations dangereuses pour la conduite semi-autonome d’un véhicule et ainsi développer de nouvelles interfaces homme-voiture afin que le conducteur puisse mieux gérer ces situations. En particulier, le but est de mesurer l’état physiologique du conducteur en temps réel afin d’adapter les informations transmises et lui permettre d’être prêt à reprendre le contrôle du véhicule. 
Des modèles de Machine Learning ont été déjà entraînés, sur la base de données physiologiques de plus de 200 personnes récoltées lors de plusieurs expériences sur un simulateur de conduite. Les modèles ont été entraînés en utilisant le framework Scikit-learn en Python. 
Les modèles permettent de prédire l'état du conducteur selon plusieurs composantes :
 - stress (présence d'une personne, environnement de conduite stressant)
 - relaxation (état physiologique calme)
 - charge cognitive (réalisation d'une tâche secondaire pendant que la voiture roule de manière autonome)
 - fatigue (privation de sommeil)
 - conscience de la situation de conduite (situation awareness en anglais)

Le but de ce projet est de développer un algorithme permettant de fusionner la prédiction de ces différents classificateurs afin d'obtenir un indicateur global de l’état du conducteur en temps réel. Comme objectif secondaire, un système de visualisation de l’état du conducteur pourra aussi être implémentée, afin de visualiser l'état global du conducteur à l’aide d’une icone.


## Objectifs

Pendant le projet l’étudiant devra accomplir les tâches suivantes :

1. Se familiariser avec les signaux et capteurs physiologiques ainsi qu'avec le machine learning (classification)
2. Analyse : faire un état de l'art des techniques permettant de fusionner la prédiction de plusieurs classificateurs
3. Analyse : faire une analyse du poids à appliquer à chaque classificateur (c'est à dire à chaque composante)
4. Conception : faire une architecture de l'algorithme à implémenter
5. Implémentation de l'algorithme de fusion
6. Test de l'algorithme avec plusieurs personnes dans un simulateur de conduite
7. Optionnel : Réalisation d'une visualisation de la prédiction de l'algorithme à l'aide d'une icône sur le tableau de bord du simulateur

## Contraintes

- Utiliser Python pour implémenter l'algorithme de fusion
