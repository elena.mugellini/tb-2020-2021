---
version: 2
type de projet: Travail de bachelor
année scolaire: 2020/2021
titre: Application pour l'autosoin de la Sclérose En Plaques
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Elena Mugellini
  - Leonardo Angelini
proposé par étudiant: Corentin Bompard
mots-clés: [autosoins, Scélorose En Plaques, app, machine learning]
langue: [F,E]
confidentialité: non
suite: oui
---

## Contexte

Dans le cadre du projet de semestre 5 et 6, en collaboration avec des étudiants de la Haute Ecole de Santé, une application pour l'autosoin des la sclérose en plaques a été développée et sera testée avec des utilisateurs. Le but de ce projet est d'implementer une version améliorée de l'application existante, afin de prendre en compte les retours des utilisateurs, ainsi que de dévélopper des algorithmes d'apprentissage automatique pour l'analyse des données rentrées dans l'app.


## Objectifs
- Analyse des retours utilisateurs
- Analyse statistique des données
- Conception et implementation d'un algorithme pour la detection des phases de la maladie 
- Implementation des améliorations dans l'app
- Tests utilisateur 

## Contraintes
Suite du projet de semestre 6 en collaboration avec la HEdS
